package com.tehdannydarko;

import javax.swing.JFrame;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

public class Game {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private String gameName = "Flappy Bird";

    private Canvas game = new Canvas();

    private Input input;

    private ArrayList<Updateable> updateables = new ArrayList<>();
    private ArrayList<Renderable> renderables = new ArrayList<>();

    public void addUpdateable(Updateable u) {
        this.updateables.add(u);
    }

    public void removeUpdateable(Updateable u ) {
        this.updateables.remove(u);
    }

    public void addRenderable(Renderable r) {
        this.renderables.add(r);
    }

    public void removeRenderable(Renderable r) {
        this.renderables.remove(r);
    }

    public void start() {
        // init window
        Dimension gameSize = new Dimension(Game.WIDTH, Game.HEIGHT);
        JFrame gameWindow = new JFrame(this.gameName);
        gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameWindow.setSize(gameSize);
        gameWindow.setResizable(false);
        gameWindow.setVisible(true);

        this.game.setSize(gameSize);
        this.game.setMinimumSize(gameSize);
        this.game.setMaximumSize(gameSize);
        this.game.setPreferredSize(gameSize);

        gameWindow.add(this.game);
        gameWindow.setLocationRelativeTo(null);

        // init input
        this.input = new Input();
        this.game.addKeyListener(this.input);
        this.game.requestFocus();

        // game loop
        final int TICKS_PER_SECOND = 60;
        final int TIME_PER_TICK = 1000 / TICKS_PER_SECOND;
        final int MAX_FRAMESKIPS = 5;

        long nextGameTick = System.currentTimeMillis();
        int loops;
        float interpolation;

        long timeAtLastFpsCheck = 0;
        int ticks = 0;

        boolean running = true;
        while(running) {
            // update
            loops = 0;
            while(System.currentTimeMillis() > nextGameTick && loops < MAX_FRAMESKIPS) {
                update();
                ticks++;

                nextGameTick += TIME_PER_TICK;
                loops++;
            }

            // render
            interpolation = (float) (System.currentTimeMillis() + TIME_PER_TICK - nextGameTick) /
                            (float) TIME_PER_TICK;
            render(interpolation);

            // FPS check
            if(System.currentTimeMillis() - timeAtLastFpsCheck >= 1000) {
                System.out.println("FPS: " + ticks);
                gameWindow.setTitle(gameName + " - FPS: " + ticks);
                ticks = 0;
                timeAtLastFpsCheck = System.currentTimeMillis();
            }
        }
    }

    private void update() {
        for(Updateable u : this.updateables) {
            u.update(this.input);
        }
    }

    private void render(float interpolation) {
        BufferStrategy strategy = this.game.getBufferStrategy();
        if(strategy == null) {
            this.game.createBufferStrategy(2);
            return;
        }

        Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
        g.clearRect(0, 0, this.game.getWidth(), this.game.getHeight());

        for(Renderable r : this.renderables) {
            r.render(g, interpolation);
        }

        g.dispose();
        strategy.show();
    }
}

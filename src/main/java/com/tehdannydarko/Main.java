package com.tehdannydarko;

public class Main {

    public static void main(String[] args) {
        Game g = new Game();

        // initialize game objects
        Pipes p = new Pipes();
        Bird b = new Bird(p);

        // add updateables and renderables
        g.addRenderable(p);
        g.addUpdateable(p);

        g.addRenderable(b);
        g.addUpdateable(b);

        // start!
        g.start();
    }
}

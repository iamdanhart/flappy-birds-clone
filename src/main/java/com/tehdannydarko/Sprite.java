package com.tehdannydarko;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Sprite {

    public static BufferedImage getSprite(String filename)
            throws IOException {
        return ImageIO.read(
                Sprite.class.getClassLoader().getResourceAsStream(filename));
    }
}
